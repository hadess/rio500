Preparations
------------

* The userspace driver now requires libusb-0.1.3 or higher.  As of this
  writing, the current version is 0.1.3b.  This can be downloaded from
  http://sourceforge.net/projects/libusb/ 

* The userspace driver has only be tested on 2.3.99-pre3 kernels
  and up.  If you are choosing this option, it is highly recommended that
  you kernel version meet this minimum.  The userspace driver did not
  function properly on earlier development kernels.

* The userspace driver relies on Linux usb support together with the 
  usbdevfs to provide access to usb devices.  In order to use the
  userspace driver, first you must make sure that usb support is loaded.
  Next, usbdevfs support must be compiled into your kernel and the 
  usbdevfs mounted.  Currently, you may mount the usbdevfs with default
  permissions, which will require you to be root to run the rio utils or
  you may set more lax permissions for the entire usbdevfs.  To keep the
  permissions tight, use
	 mount -t usbdevfs none /proc/bus/usb

* Some udev and ConsoleKit parameters files are installed for systems
  that use those to allow accessing the device as a normal user on the
  console. If they do not work, get your distribution to contact us for
  more specific instructions.

* In addition you will need to configure usbdevfs support into the
  rio500 tools.  This will be discussed below.


Configuring and building the tools
----------------------------------

./configure --sysconfdir=/etc/ --prefix=/usr

** Use ./configure --help to see available configure options.
If you want to utilize the new ID3support, you must
configure it using --with-id3support.  Since this is a new option, it
is not set by default **


make

make install


Available utilities
-------------------
rio_format
rio_stat
rio_font_info
rio_add_folder
rio_add_song
rio_del_song
rio_get_song

Help is available for any of these utilities using --help

