/*  ----------------------------------------------------------------------

    Copyright (C) 2000  Cesar Miquel  (miquel@df.uba.ar)
                  2001  Bastien Nocera (hadess@hadess.net)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    ---------------------------------------------------------------------- */

#include "config.h"

#include <glib/gi18n-lib.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include "librio500_api.h"
#include "getopt.h"

void get_some_switches (int argc, char *argv[]);
void usage (char *progname);

void
usage (char *progname)
{
  g_print (_("\nusage: %s [OPTIONS] \n"), progname);
  g_print ("\n");
  g_print (_("\n Shows Rio statistics, such as:\n"));
  g_print (_("\n   Memory free"));
  g_print (_("\n   Folder Names"));
  g_print (_("\n   Stored songs"));
  g_print (_("\n   Song sizes\n"));
  g_print ("\n");
  return;
}

void show_songs (Rio500 * rio, GList * songs);

int
main (int argc, char *argv[])
{
  unsigned long memfree, memtotal;
  GList *content, *item;
  RioFolderEntry *entry;
  int card, card_count;
  Rio500 *rio;

  bindtextdomain (PACKAGE, PACKAGE_LOCALE_DIR);
  textdomain (PACKAGE);

  get_some_switches (argc, argv);

  /* Open connection to rio */
  rio = rio_new ();
  if (rio_check (rio) == FALSE) {
    g_print (_("init_communication() failed!\n"));
    exit (-1);
  }
  {
    int major, minor;

    rio_get_revision (rio, &major, &minor);
    g_print (_("Your Rio500 has firmware revision %d.%02d\n"),
	     major, minor);
  }

  card_count = rio_get_card_number (rio);

  for (card = 0; card < card_count; card++) {
    rio_set_card (rio, card);

    /* Check how much memory we have */
    memfree = rio_memory_left (rio);
    memtotal = rio_get_mem_total (rio);

    content = rio_get_content (rio);

    g_print (_
	     ("Card %d reports %ld Mb free (%ld bytes) out of %ld Mb (%ld bytes).\n"),
	     card, (memfree / (1024 * 1024)), memfree,
	     (memtotal / (1024 * 1024)), memtotal);

    g_print
	("-------------------------------------------------------------\n");
    g_print ("  N   num songs       Folder Name\n");
    g_print
	("-------------------------------------------------------------\n");

    for (item = content; item; item = item->next) {
      entry = (RioFolderEntry *) item->data;
      if (entry) {
	g_print
	    (_("(%02d)     %2d           %s\n"),
	     entry->folder_num, g_list_length (entry->songs), entry->name);
	show_songs (rio, entry->songs);
      }
    }
    if (content != NULL)
      rio_destroy_content (content);
  }
  rio_delete (rio);
  exit (0);
}

void
show_songs (Rio500 * rio, GList * songs)
{
  GList *item;
  RioSongEntry *entry;
  int song_num;

  g_print ("\n");
  g_print (_("    (num)  size           song name\n"));

  for (item = songs; item; item = item->next) {
    entry = (RioSongEntry *) item->data;
    if (entry) {
      song_num = entry->song_num++;
      g_print (_("    (%2d) (%8ld bytes) %s\n"),
	       entry->song_num-1, entry->size, entry->name);
    }
  }
  g_print ("\n\n");
}

static char const shortopts[] = "hv";

static struct option const longopts[] = {
  {"version", no_argument, NULL, 'v'},
  {"help", no_argument, NULL, 'h'},
  {NULL, no_argument, NULL, 0}
};

static char const *const option_help[] = {
  N_("Miscellaneous options:"),
  "",
  N_("  -v        --version          Output version info."),
  N_("  -h        --help             Output this help."),
  "",
  N_("Report bugs to <rio500-devel@lists.sourceforge.net>."),
  0
};


/* Process switches and filenames.  */

void
get_some_switches (int argc, char *argv[])
{
  register int optc;
  char const *const *p;

  if (optind == argc)
    return;
  while ((optc = getopt_long (argc, argv, shortopts, longopts, (int *) 0))
	 != -1) {
    switch (optc) {
    case 'v':
      g_print (_("\nrio_stat -- version %s\n"), VERSION);
      exit (0);
      break;
    case 'h':
      usage (argv[0]);
      for (p = option_help; *p; p++)
	g_print ("%s\n", **p ? gettext (*p) : *p);
      exit (0);
      break;
    default:
      usage (argv[0]);
      for (p = option_help; *p; p++)
	g_print ("%s\n", **p ? gettext (*p) : *p);
      exit (0);
      break;

    }
  }
}
