/*
 *  Rio500 Startup Animation Data Converter
 *
 *  Copyright (C) 2000  Iwasa Kazmi
 *                2001  Bastien Nocera <hadess@hadess.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * 2000/08/05  v0.3
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <bmp2rioani.h>

#include "getopt.h"

#ifdef DEBUG
#define DEBUGF(x)  printf x
#else
#define DEBUGF(x)
#endif

void
title(void)
{
  fprintf(stderr, "Rio500 Startup Animation File Converter\n");
  fprintf(stderr, "Copyright(C) 2000 Iwasa Kazmi\n");
  fprintf(stderr, "\n");
}


void
usage(char **argv)
{
  fprintf(stderr, "  %s  [options]  bmp  ani\n", argv[0]);
  fprintf(stderr, "\n");
  fprintf(stderr, "  options:\n");
  fprintf(stderr, "    -d n  --delay n       : delay between frame (1/20s)\n");
  fprintf(stderr, "    -f n  --final-delay n : delay after final frame (1/20s)\n");
  fprintf(stderr, "                           (if loop > 0, final-delay is ignored)\n");
  fprintf(stderr, "    -l n  --loop n        : animation loop count\n");
  fprintf(stderr, "    -c    --clear-lcd     : clear LCD at startup\n");
  fprintf(stderr, "    -C s  --comment s     : comment\n");
  fprintf(stderr, "\n");
  fprintf(stderr, "  files:\n");
  fprintf(stderr, "    bmp : source BMP file\n");
  fprintf(stderr, "    ani : output rio500 animation file\n");
  fprintf(stderr, "\n");
  fprintf(stderr, "  default settings:\n");
  fprintf(stderr, "    delay = 1 (1/20s)\n");
  fprintf(stderr, "    final-delay = 4 (4/20s)\n");
  fprintf(stderr, "    loop = 0\n");
  fprintf(stderr, "    clear LCD at startup = off\n");
  fprintf(stderr, "\n");

  exit(1);
}



static
struct option long_opt[] = {
  { "help", no_argument, NULL, 0 },
  { "frames", required_argument, NULL, 'n' },
  { "delay", required_argument, NULL, 'd' },
  { "final-delay", required_argument, NULL, 'f' },
  { "loop", required_argument, NULL, 'l' },
  { "clear-lcd", no_argument, NULL, 'c' },
  { "comment", required_argument, NULL, 'C' },
  { NULL, 0, NULL, 0 },
};

static const char short_opt[] = "n:d:f:l:cC:";

int
main(int argc, char **argv)
{
  int delay = 1;
  int final_delay = 4;
  int loop_count = 0;
  gboolean clear_lcd = FALSE;
  gchar *comment = NULL;
  gchar *bmpfile, *outfile;
  int op_framenum, r;

  title();

  if (argc < 3)
    usage(argv);

  for(;;)
    {
      int c = getopt_long(argc, argv,
                     short_opt, long_opt, NULL);
      if (c == -1)
        break;

      switch(c)
        {
        case 'n':
          op_framenum = atoi(optarg);
          DEBUGF(("opt: frames = %d\n", op_framenum));
          break;

        case 'd':
          delay = atoi(optarg);
          DEBUGF(("opt: delay = %d\n", delay));
          break;

        case 'f':
          final_delay = atoi(optarg);
          DEBUGF(("opt: final delay = %d\n", final_delay));
          break;

        case 'l':
          loop_count = atoi(optarg);
          DEBUGF(("opt: loop_count = %d\n", loop_count));
          break;

        case 'c':
          clear_lcd = 1;
          DEBUGF(("opt: clear LCD\n"));
          break;

        case 'C':
	  comment = g_strdup(optarg);
          DEBUGF(("opt: comment = '%s'\n", comment));
          break;

        default:
          usage(argv);
        }
    }

  if (optind + 1 >= argc)
    usage(argv);

  bmpfile = g_strdup(argv[optind]);
  outfile = g_strdup(argv[optind+1]);

  if (delay < 0 || delay > 20)
    {
      fprintf(stderr, "invalid delay between frame. set 0 - 20\n");
      return 1;
    }
  if (final_delay < 1 || final_delay > 100)
    {
      fprintf(stderr, "invalid delay of final frame. set 1 - 100\n");
      return 1;
    }
  if (loop_count < 0 || loop_count > 99)
    {
      fprintf(stderr, "invalid loop count. set 0 or 1 - 99\n");
      return 1;
    }

  if (loop_count > 0)
    {
      fprintf(stderr, "information: loop mode was enabled. final delay is ignored.\n");
    }

  fprintf(stderr, "converting...");
  r = rio_bmp2rioani(delay, final_delay, loop_count,
		  clear_lcd, comment,
		  bmpfile, outfile);

  if (r != RIO_SUCCESS)
  {
    g_print("\n%s\n", rio_result_to_string(r));
    exit (-1);
  }
  fprintf(stderr, "Done.\n");

  DEBUGF(("exit %d\n", r));
  return r;
}

