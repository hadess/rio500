/*  ----------------------------------------------------------------------

    Copyright (C) 2001  Bastien Nocera  (hadess@hadess.net)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    ---------------------------------------------------------------------- */

#include "config.h"

#include <glib/gi18n-lib.h>

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

#include "getopt.h"
#include <locale.h>
#include "librio500_api.h"


int name_flag = 0;
int folder_num_set = 0;
int card_number = 0;
char *folder_num_string = NULL;
char numstring[4];

char * int_to_string(int x);
void usage (char *progname);
int main(int argc, char *argv[]);
void get_some_switches (int argc, char *argv[], int *folder_num, int *card);


void
usage (char *progname)
{
  printf (_("\nusage: %s [OPTIONS] <song1> <song2>\n"), progname);
  printf (_("\n [OPTIONS] Try --help for more information\n"));
  printf (_("\n <songN> is the index or name of the song you"));
  printf (_("\n want to swap."));
  printf ("\n\n");
  return;
}

int
main(int argc, char *argv[])
{
  int		    first_song,second_song, folder_num;
  int		    rio_num_songs=0;
  GList		    *folder = NULL;
  RioFolderEntry    *rio_folder = NULL;
  GList		    *rio_content;
  GList 	    *rio_songs;
  Rio500	    *rio_dev;

  setlocale (LC_MESSAGES, "");
  bindtextdomain (PACKAGE, PACKAGE_LOCALE_DIR);
  textdomain (PACKAGE);


  /* Process switches */
  get_some_switches(argc,argv,&folder_num,&card_number);


  /* Make sure folder_num isn't set to something stupid with -F */
  if (folder_num < 0 || folder_num>256) /* new limit is 256 folders */
  {
    printf (rio_result_to_string(RIO_NODIR));
    usage (argv[0]);
    exit (-1);
  }

  /* Sanity check -- make sure there is a file list to swap */
  if (optind == argc)
  {
	printf(_("\nNeed to specify a file name/index to delete\n"));
	usage(argv[0]);
	exit(-1);
  }
  first_song = atoi(argv[optind++]);
  second_song = atoi(argv[optind]);

  rio_dev = rio_new();
  if (rio_check(rio_dev) == FALSE)
  {
	  g_print (_("init_communication() failed!\n"));
	  exit (-1);
  }
  rio_set_card(rio_dev, card_number);
  rio_content = rio_get_content(rio_dev);
  if (rio_content == NULL || folder_num >= g_list_length(rio_content))
  {
	  rio_destroy_content(rio_content);
	  rio_delete(rio_dev);
	  g_print("%s\n", rio_result_to_string(RIO_NODIR));
	  exit (-1);
  }
  folder = g_list_nth(rio_content, folder_num);
  rio_folder = (RioFolderEntry *)(folder->data);
  rio_songs = rio_folder->songs;
  rio_num_songs = g_list_length(rio_songs);
  if (rio_num_songs < first_song || rio_num_songs < second_song)
  {
	  rio_destroy_content(rio_content);
	  rio_delete(rio_dev);
	  g_print("%s\n", rio_result_to_string(RIO_FILEERR));
	  exit(-1);
  }
  
  rio_swap_songs(rio_dev, folder_num, first_song, second_song);

  rio_destroy_content(rio_content);
  rio_delete(rio_dev);
  exit (1);
}

static char const shortopts[] = "F:xhv";
static struct option const longopts[] =
{
  {"folder", required_argument, NULL, 'F'},
  {"external", no_argument, NULL, 'x'},
  {"version", no_argument, NULL, 'v'},
  {"help", no_argument, NULL, 'h'},
  {NULL, no_argument, NULL, 0}
};

static char const *const option_help[] =
{
N_("Input options:"),
"",
N_("  -F x      --folder x         Swap songs in folder of index x"),
N_("  -x        --external         Swap songs on external memory card"),
"",
N_("Miscellaneous options:"),
"",
N_("  -v        --version          Output version info."),
N_("  -h        --help             Output this help."),
"",
N_("Report bugs to <rio500-devel@lists.sourceforge.net>."),
0
};


/* Process switches and filenames.  */

void
get_some_switches (int argc, char *argv[], int *folder_num, int *card_number)
{
    register int optc;
    char const * const *p;

    if (optind == argc)
        return;
    while ((optc = getopt_long (argc, argv, shortopts, longopts, (int *) 0))
           != -1) {
         switch (optc) {
	    case 'x':
		*card_number=1;
		break;
            case 'F':
                folder_num_set = 1;
		folder_num_string=malloc(sizeof(optarg));
		folder_num_string=strcpy(folder_num_string,optarg);
		*folder_num=atoi(folder_num_string);
                break;
            case 'v':
                printf(_("\nrio_swap_songs -- version %s\n"),VERSION);
                exit(0);
                break;
            case 'h':
                usage(argv[0]);
                for (p=option_help;  *p ;  p++)
                  fprintf (stderr, "%s\n", *p);
                exit(0);
                break;
            default:    
		usage (argv[0]);
        }
    }
}

