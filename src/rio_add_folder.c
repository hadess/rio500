/*  ----------------------------------------------------------------------

    Copyright (C) 2000  Cesar Miquel  (miquel@df.uba.ar)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    ---------------------------------------------------------------------- */

#include "config.h"

#include <glib/gi18n-lib.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>
#include <fcntl.h>
#include <errno.h>

#include <locale.h>
#include "getopt.h"
#include "librio500_api.h"

void
get_some_switches (int argc, char *argv[], int *font_number, int *card_number);

char *font_name = DEFAULT_FON_FONT;
char *temp_name;

void
usage (char *progname)
{
  printf (_("\nusage: %s [OPTIONS] <folder_name1>  . . <folder_name256>\n"), progname);
  printf (_("\n [OPTIONS]  Try --help for more information"));
  printf (_("\n <folder_nameN> is the name of the folder we want to create\n"));
  printf (_("\n Currently, the maximum number of supported folders is 256\n"));
  printf (_("\n"));
  return;
} 

int
main(int argc, char *argv[])
{
  int font_number=0,card_number=0;
  char *foldername;
  Rio500 *rio;

  setlocale (LC_MESSAGES, "");
  bindtextdomain (PACKAGE, PACKAGE_LOCALE_DIR);
  textdomain (PACKAGE);

/* Check arguments */

  get_some_switches(argc,argv,&font_number,&card_number);

  rio = rio_new();

  if (rio_check(rio) == FALSE)
  {
     printf(_("init_communication() failed!\n"));
     exit (-1);
  }

// Setup internal/external card and font then start adding folders

  rio_set_card(rio, card_number);
  rio_set_font(rio, font_name, font_number);

  while (optind < argc)  /* loop through filenames and add */
  {
  int res;
  foldername=malloc(33);
  foldername=safe_strcpy(foldername,argv[optind++],32);

   /* Create new folder */
   printf(_("Adding folder %s\n"),foldername);
   res = rio_add_folder (rio, foldername);

   if (res != RIO_SUCCESS)
   {
     g_print("%s\n", rio_result_to_string(res));
     free(foldername);
     rio_delete(rio);
     exit (-1);
   }

   free(foldername);
   } /* end of while loop */
   rio_delete(rio);
   exit (0);
}


// Switch processing code from the point down

static char const shortopts[] = "xf:n:hv";
static struct option const longopts[] =
{
  {"external", no_argument, NULL, 'x'},
  {"fontname", required_argument, NULL, 'f'},
  {"fontnumber", required_argument, NULL, 'n'},
  {"version", no_argument, NULL, 'v'},
  {"help", no_argument, NULL, 'h'},
  {NULL, no_argument, NULL, 0}
};

static char const *const option_help[] =
{
N_("Input options:"),
"",
N_("  -x        --external         Add folder to external memory card"),
N_("  -f name   --fontname name    Set the fontname to be used on the Rio display."),
N_("  -n x      --fontnumber x     Set the fontnumber within the given "),
N_("                               .fon file set with -f"),
"",
N_("Miscellaneous options:"),
"",
N_("  -v        --version          Output version info."),
N_("  -h        --help             Output this help."),
"",
N_("Report bugs to <rio500-devel@lists.sourceforge.net>."),
0
};


/* Process switches and filenames.  */

void
get_some_switches (int argc, char *argv[], int *font_number, int *card_number)
{
    register int optc;
    char const * const *p;
    FILE *fptemp=0;
    int newpathsize = 0;
    int newfontsize = 0;
    char *temp_fullpath_name;
    char *temp_font_name;

    if (optind == argc)
        return;
    while ((optc = getopt_long (argc, argv, shortopts, longopts, (int *) 0))
           != -1) {
         switch (optc) {
	    case 'x':
		*card_number=1;
		break;

            case 'f':
                newpathsize = strlen(DEFAULT_FONT_PATH)+strlen(optarg)+1;
                temp_fullpath_name=(char *)malloc(newpathsize);
                strcpy(temp_fullpath_name,DEFAULT_FONT_PATH);
                strcat(temp_fullpath_name,optarg);

		newfontsize = strlen(optarg)+1;
		temp_font_name=(char *)malloc(newfontsize);
		strcat(temp_font_name,optarg);

                if ( ( fptemp = fopen(temp_fullpath_name, "rb" ) ) == 0 )
                {
                 fprintf(stderr,_("\n%s is an invalid fontpath/fontname\n"),temp_fullpath_name);
		 free(temp_fullpath_name);
		 free(temp_font_name);
                 exit(-1);
                 break;
                }
                else
                {
                 fclose(fptemp);
		 font_name=temp_font_name;
                 break;
                }

            case 'n':
                /* Sanity check --fontnumber digit */
                if(!isdigit(*optarg)) {
                   fprintf(stderr,_("\nFont number must be numeric!\n"));
                   usage(argv[0]);
                   exit(-1);
                   break;
                }
                *font_number = atoi(optarg);
                break;
            case 'v':
                printf(_("\nrio_add_folder -- version %s\n"),VERSION);
                exit(0);
                break;
            case 'h':
                usage(argv[0]);
                for (p=option_help;  *p ;  p++)
                  fprintf (stderr, _("%s\n"), **p ? gettext(*p) : *p);
                exit(0);
                break;
            default:
                usage (argv[0]);
        }
    }

    /* Processing any filename args happens in main code.  */

}



