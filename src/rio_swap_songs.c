/*  ----------------------------------------------------------------------

    Copyright (C) 2001  Keith Clayton  (keith@claytons.org)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    ---------------------------------------------------------------------- */

#include "config.h"

#include <glib/gi18n-lib.h>

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

#include "getopt.h"
#include <locale.h>
#include "librio500_api.h"


#ifndef TRUE

#define TRUE 				1
#define FALSE 				0

#endif /* TRUE */

int name_flag = 0;
int folder_num_set = 0;
int card_number = 0;
char *folder_num_string = NULL;
char numstring[4];

static gint   g_alpha_sort (gconstpointer a, gconstpointer b);
char * int_to_string(int x);
void usage (char *progname);
int main(int argc, char *argv[]);
void get_some_switches (int argc, char *argv[], int *folder_num, int *card);


void
usage (char *progname)
{
  printf (_("\nusage: %s [OPTIONS] <song1> . . . <songN>\n"), progname);
  printf (_("\n [OPTIONS] Try --help for more information\n"));
  printf (_("\n <songN> is the index or name of the song you"));
  printf (_("\n want to swap."));
  printf ("\n\n");
  return;
}

int
main(int argc, char *argv[])
{
  int		    first_song,last_song = 0;
  int               count,counter,song_num1,song_num2,folder_num;
  int               num_songs_to_swap,temp,temp2,i,index=0;
  int		    rio_num_songs=0;
  //char		    *song_num_string=NULL;
  char		    *temptr = NULL;
  char		    *temptr2 = NULL;
  GList		    *current_song_order = NULL;
  GList		    *final_song_order = NULL;
  RioFolderEntry    *rio_folder = NULL;
  //char              answer[255];
  //folder_entry *    folder_ent;
  //song_entry *	    song_ent;
  GList		    *rio_content;
  GList 	    *rio_songs;
  Rio500	    *rio_dev;

  setlocale (LC_MESSAGES, "");
  bindtextdomain (PACKAGE, PACKAGE_LOCALE_DIR);
  textdomain (PACKAGE);


/* Some quick checking before we process switches */

  if (argc < 2)
  {
    usage (argv[0]);
    exit (-1);
  }


  folder_num=song_num1=song_num2=num_songs_to_swap=0;

  /* Process switches */
  get_some_switches(argc,argv,&folder_num,&card_number);


  /* Make sure folder_num isn't set to something stupid with -F */
  if (folder_num < 0 || folder_num>256) /* new limit is 256 folders */
  {
    printf (_("Non-existent folder\n"));
    usage (argv[0]);
    exit (-1);
  }

  /* Sanity check -- make sure there is a file list to swap */
  if (optind == argc)
  {
	printf(_("\nNeed to specify a file name/index to delete\n"));
	usage(argv[0]);
	exit(-1);
  }

  /* grab the swap order */
  /* Need to be sure that largest index given really exists on rio */
  while (optind < argc)
  {
	  final_song_order = g_list_append(final_song_order,argv[optind++]);
          num_songs_to_swap++;
  }

  /* check that num songs to swap = num songs on the rio */

  printf (_("Num of songs to swap = %d\n"),num_songs_to_swap);

  rio_dev = rio_new();
  rio_set_card(rio_dev,card_number);
  rio_content = rio_get_content(rio_dev);
  rio_folder = (RioFolderEntry *)(g_list_nth(rio_content,folder_num)->data);
  rio_songs = rio_folder->songs;
  rio_num_songs = g_list_length(rio_songs);
  printf(_("Number of songs in folder is %d\n"),rio_num_songs);

  /* Sanity check . . num_songs_to_swap = number of songs in the folder */
  if (rio_num_songs != num_songs_to_swap) {
	printf(_("All songs in folder %d of your rio\n"),folder_num);
	printf(_("have not been accounted for in your\n"));
	printf(_("list of songs to swap\n\n"));
	printf(_("Please check your song list and try again\n"));
	exit(-1);
  }

  for(count = 0; count < num_songs_to_swap; count++)
  {
	printf(_("Entered: %s\n"),(char *)(g_list_nth(final_song_order,count)->data));
  }

	current_song_order = g_list_copy(final_song_order);

  /* sort the glist to create numerically ordered song_list */
	  current_song_order = g_list_sort(current_song_order,g_alpha_sort);

  /* Sanity check . . if there are 5 songs to swap, current song order should
     start with 0 and end with 4  . . check that current song order starts
     with 0 and ends with num_songs_to_swap */

  first_song = atoi((char *)(g_list_nth(current_song_order,0)->data));
  last_song = atoi((char *)(g_list_nth(current_song_order,(num_songs_to_swap-1))->data));
  
  if (first_song != 0 || last_song != (num_songs_to_swap-1)) {
    printf(_("You've entered an improper song list to swap\n"));
    printf(_("Check that all songs are accounted for and that\n"));
    printf(_("your inidices range from 0 through n-1\n"));
    exit(-1);
  }

  /* Sanity check . . make sure all songs are accounted for */

  for (count=0; count < num_songs_to_swap; count++) {
	if (count != (atoi((char *)g_list_nth(current_song_order,count)->data)))	{
		printf(_("You are missing song index %d\n"),count);
		printf(_("From your list of songs to swap\n"));
		printf(_("Please check your song list and try again\n"));
		exit(-1);
	}
  }
  /* Check the ordering */
  for(count = 0; count < num_songs_to_swap; count++)
  {
	printf(_("Original: %s  Ordered: %s\n"),(char *)(g_list_nth(final_song_order,count)->data),(char *)(g_list_nth(current_song_order,count)->data));
  }



  for(count = 0; count < num_songs_to_swap; count++)
  {
	song_num1 = count;
	temp = atoi((char *)(g_list_nth(final_song_order,count)->data));
	/* find index of temp song in current_song_order */
	for (counter = 0; counter < num_songs_to_swap; counter++)
	{
		temp2 = atoi((char *)(g_list_nth(current_song_order,counter)->data));
	  	if (temp == temp2) index=counter;
	}
	song_num2 = index;
	
	/* if song_num1 == song_num2 we don't need to do anything */
	if (song_num1 == song_num2) continue;
	rio_swap_songs(rio_dev,folder_num,song_num1,song_num2);
	/* update current_song_order */
	/* need to interchange count and counter elements */
	temptr = (char *)(g_list_nth(current_song_order,index)->data);
	temptr2 = (char *)(g_list_nth(current_song_order,count)->data);
	/* remove from back of the list first */	
	if (count < index) {
		current_song_order = g_list_remove(current_song_order,temptr);
		current_song_order = g_list_remove(current_song_order,temptr2);
	} else {
                current_song_order = g_list_remove(current_song_order,temptr2);
                current_song_order = g_list_remove(current_song_order,temptr);
	}	
	/* put swapped values back in starting at front of list */
	if (count < index) {
		current_song_order = g_list_insert(current_song_order,temptr,count);
		current_song_order = g_list_insert(current_song_order,temptr2,index);
	} else {
                current_song_order = g_list_insert(current_song_order,temptr2,index);
                current_song_order = g_list_insert(current_song_order,temptr,count);
	}	
	
	/* test loop to see that interchange happening correctly */
        printf("Current song ordering:\n"); 
	for(i = 0; i < num_songs_to_swap; i++) {
		printf("%s\n",(char *)(g_list_nth(current_song_order,i)->data));
        }
  }
   rio_delete(rio_dev);
   exit (1);
}

static char const shortopts[] = "F:xhv";
static struct option const longopts[] =
{
  {"folder", required_argument, NULL, 'F'},
  {"external", no_argument, NULL, 'x'},
  {"version", no_argument, NULL, 'v'},
  {"help", no_argument, NULL, 'h'},
  {NULL, no_argument, NULL, 0}
};

static char const *const option_help[] =
{
N_("Input options:"),
"",
N_("  -F x      --folder x         Swap songs in folder of index x"),
N_("  -x        --external         Swap songs on external memory card"),
"",
N_("Miscellaneous options:"),
"",
N_("  -v        --version          Output version info."),
N_("  -h        --help             Output this help."),
"",
N_("Report bugs to <rio500-devel@lists.sourceforge.net>."),
0
};


/* Process switches and filenames.  */

void
get_some_switches (int argc, char *argv[], int *folder_num, int *card_number)
{
    register int optc;
    char const * const *p;
    //FILE *fptemp=0;

    if (optind == argc)
        return;
    while ((optc = getopt_long (argc, argv, shortopts, longopts, (int *) 0))
           != -1) {
         switch (optc) {
	    case 'x':
		*card_number=1;
		break;
            case 'F':
                folder_num_set = 1;
		folder_num_string=malloc(sizeof(optarg));
		folder_num_string=strcpy(folder_num_string,optarg);
		*folder_num=atoi(folder_num_string);
                break;
            case 'v':
                printf(_("\nrio_swap_songs -- version %s\n"),VERSION);
                exit(0);
                break;
            case 'h':
                usage(argv[0]);
                for (p=option_help;  *p ;  p++)
                  fprintf (stderr, "%s\n", *p);
                exit(0);
                break;
            default:    
		usage (argv[0]);
        }
    }

    /* Processing any filename args happens in main code.  */

}

gint g_alpha_sort (gconstpointer a, gconstpointer b)
{
  int x,a_len,b_len;
  
  a_len = strlen(a);   /* Want to sort in descending order */
  b_len = strlen(b);   /* so return value will be same as */
  if (a_len != b_len)  /* normal strcmp . . also 10 should */
  {
    if (a_len > b_len) /* come after 9 so check length too */
    {
      return (1);
    }
    else
    {
      return (-1);
    }
  }              
  x=strcmp(a,b);
  return (x);
}
