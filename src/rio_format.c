/*  ----------------------------------------------------------------------

    Copyright (C) 2000  Cesar Miquel  (miquel@df.uba.ar)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    ---------------------------------------------------------------------- */

#include "config.h"

#include <glib/gi18n.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

#include <locale.h>
#include "librio500.h"
#include "getopt.h"

void get_some_switches (int argc, char *argv[], int * automat, int *format_internal, int *format_external);
void usage (char *progname);

void
usage (char *progname)
{
  printf (_("\nusage: %s [OPTIONS] \n"), progname);
  printf ("\n");
  printf (_("\n Formats the Rio flash memory, erasing all songs and folders\n"));
  printf("\n");
return;
}

int 
main(int argc, char *argv[])
{
  char answer[255];
  int automatic = 0;
  int format_internal = 0;
  int format_external = 0;

  usb_dev_handle *rio_dev;

  setlocale (LC_MESSAGES, "");
  bindtextdomain (PACKAGE, PACKAGE_LOCALE_DIR);
  textdomain (PACKAGE);

  get_some_switches(argc,argv,&automatic,&format_internal,&format_external);

  /* if no switches set, default to formatting internal memory */

  if (!format_internal && !format_external) {
	format_internal=1;
  }
  if (!automatic)
    {
      /* Issue a warning */
      printf ("\n\n");
      printf ("---------------------------------------------------------\n");
      printf (_("                  W A R N I N G\n"));
      printf ("---------------------------------------------------------\n");
      printf ("\n");
      printf (_("This command will erase ALL your folders and songs stored\n"));
      printf (_("on your RIO 500's "));

      if (format_internal)
        printf(_("internal "));
      if (format_internal && format_external)
        printf(_("and "));
      if (format_external)
        printf(_("external "));

      printf (_("memory.\n\nAnswer with yes if you want to continue? "));
 
      scanf ("%s", answer);

      if ((strcmp (answer, "yes") != 0) && (strcmp (answer, _("yes")) != 0))  {
	exit(0);
      }
    }

   if(!(rio_dev = init_communication())) {
     printf(_("init_communication() failed!\n"));
     return -1;
   }

   send_command (rio_dev, 0x42, 0x0, 0x0);
   send_command (rio_dev, 0x42, 0x0, 0x0);
   send_command (rio_dev, 0x42, 0x0, 0x0);

   if (format_external && query_card_count(rio_dev) > 1)
   {
     printf(_("Formatting external memory card...\n"));
     format_flash (rio_dev, 1);
     printf(_("Done!\n"));
   }
   else if (!format_internal)
     printf(_("Unable to find an external memory card to format.\n"));

   if (format_internal)
   {
     printf(_("Formatting internal memory card...\n"));
     format_flash (rio_dev, 0);
     printf(_("Done!\n"));
   }

   /* Close device */
   finish_communication (rio_dev);

   exit (0);
}

static char const shortopts[] = "ahvxib";

static struct option const longopts[] =
{
  {"automatic", no_argument, NULL, 'a'},
  {"external", no_argument, NULL, 'x'},
  {"internal", no_argument, NULL, 'i'},
  {"both", no_argument, NULL, 'b'},
  {"version", no_argument, NULL, 'v'},
  {"help", no_argument, NULL, 'h'},
  {NULL, no_argument, NULL, 0}
};

static char const *const option_help[] =
{
N_("Input options:"),
"",
N_("  -a        --automatic        Format rio without prompting"),
N_("  -x        --external         Format only the external smartmedia card"),
N_("  -i        --internal         Format only the internal memory"),
N_("  -b        --both             Format internal memory and external smartmedia card"),
"",
N_("Miscellaneous options:"),
"",
N_("  -v        --version          Output version info."),
N_("  -h        --help             Output this help."),
"",
N_("Report bugs to <rio500-devel@lists.sourceforge.net>."),
0
};


/* Process switches and filenames.  */

void
get_some_switches (int argc, char *argv[], int * automat, int *format_internal, int *format_external)
{
    register int optc;
    char const * const *p;

    if (optind == argc)
        return;
    while ((optc = getopt_long (argc, argv, shortopts, longopts, (int *) 0))
           != -1) {
         switch (optc) {
            case 'a':
                *automat = 1;
                break;
            case 'v':
                printf(_("\nrio_format -- version %s\n"),VERSION);
                exit(0);
                break;
	    case 'i':
		*format_internal=1;
		break;
	    case 'x':
		*format_external=1;
		break;
            case 'b':
		*format_internal=1;
		*format_external=1;
		break;
            case 'h':
	    default:
                usage(argv[0]);
                for (p=option_help;  *p ;  p++)
                  fprintf (stderr, "%s\n", **p ? gettext(*p) : *p);
                exit(0);
                break;

         }
    }


}


