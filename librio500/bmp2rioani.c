/*
 *  Rio500 Startup Animation Data Converter
 *
 *  Copyright (C) 2001  Bastien Nocera
 *  Copyright (C) 2000  Iwasa Kazmi
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * A source bitmap must be a Windows' BMP format file.
 * A source bitmap contains multiple frame images like below.
 * Size of each frame image is 128w x 32h.
 *
 *  +-----+-----+-    --+-----+
 *  |  1  |  2  |  ...  |  n  |
 *  +-----+-----+-    --+-----+
 *  | n+1 | n+2 |  ...  | 2*n |
 *  +-----+-----+-    --+-----+
 *     :     :             :
 *  +-----+-----+-    --+-----+
 *  |     |     |  ...  |     |
 *  +-----+-----+-    --+-----+
 *
 * Total number of frames will be detected automatically.
 * (You can override it with command line option)
 *
 * 1 / 4 / 8 / 24 bit per pixel are supported.
 * White pixel (R=255,G=255,B=255) clear LCD pixel.
 * Other colors turn LCD pixel on (black).
 */

/*
 * Rio500 Startup Animation File Format
 *
 * Note: a word data is put in the little-endian byte order.
 *
 * +0000 BYTE  0x44 ('D')
 * +0001 BYTE  0x53 ('S')
 * +0002 WORD  number of frames (1 <= n <= 128)
 * +0004 WORD  delay between frames (x1/20 sec. 0 <= n <= 20)
 * +0006 WORD  loop ON  : loop count (1 <= n <= 99)
 *             loop OFF : delay after final frame (x1/20 sec. 1 <= n <= 100)
 * +0008 WORD  0x0104 (language id ?)
 * +000A WORD  erase LCD at startup (1 or 0)
 * +000C WORD  loop mode (1 or 0)
 * +000E STR   comment (0 <= length <= 498)
 * 
 * +0200 frame data 1  (512 bytes)
 * +0400 frame data 2  (512 bytes)
 *         :
 * +xx00 frame data n  (512 bytes)
 * [EOF]
 *
 * <Frame Image Data>
 *   each byte data contains 8-pixels bit image.
 *            7      0
 *            ********
 *            ------> X
 *
 *   a frame consists of 16 x 32 byte data.
 *   first 256 bytes contains upper 16 x 16 bytes region.
 *   next 256 bytes contains lower 16 x 16 bytes region.
 *   continuous bytes are mapped like below.
 *         -------------------------------> X
 *        | [byte  0] [byte 16] [byte 32]
 *        |     :         :         :
 *        | [byte 15] [byte 31]
 *      Y V
 */

/*
 * 2000/08/05  v0.3
 */

#include "bmp2rioani.h"

#ifdef DEBUG
#define DEBUGF(x)  printf x
#else
#define DEBUGF(x)
#endif

 typedef struct tBMPHDR
{
	pu_long  filesize;
	pu_long  bitmap_offset;
	pu_long  header_size;
	pu_long  width;
	pu_long  height;
	pu_long  planes;
	pu_long  bpp;
	int     compress;
	pu_long  bitmap_size;
	pu_long  colors;

} BMPHDR;

typedef struct
{
	int blue;
	int green;
	int red;

} PALETTE_COLOR;

typedef struct tBMPPALETTE
{
	int color_n;
	PALETTE_COLOR *color;

} BMPPALETTE;


/* Private functions prototypes */
int bmp2frame(FILE *f, BMPHDR *hdr, BMPPALETTE *pal, FRAMEINFO *frm);
int dump_frames(FILE *outf, FRAMEINFO *frm);
void fput_word(FILE *f, int v);
int dump_header(FILE *outf, ANIMINFO *anim, FRAMEINFO *frm);
int conv(FILE *f, FILE *outf, int op_framenum, ANIMINFO *anim);

static size_t read_pos;


/*
 * BMP utility
 *
 */

static
pu_long
bmp_dword_value(pu_char *buff)
{
  pu_long v = 0;

  v = (pu_long)buff[3];
  v = (v << 8) + (pu_long)buff[2];
  v = (v << 8) + (pu_long)buff[1];
  v = (v << 8) + (pu_long)buff[0];

  return v;
}

static
pu_long
bmp_word_value(pu_char *buff)
{
  pu_long v = 0;
  
  v = (pu_long)buff[1];
  v = (v << 8) + (pu_long)buff[0];

  return v;
}


static
int
bmp_get_header(FILE *f, BMPHDR *hdr)
{
  pu_char buff[54];

  fseek(f, 0, SEEK_SET);
  if (fread(buff, 1, sizeof(buff), f) != sizeof(buff))
    return 1;
  read_pos = sizeof(buff);

  if (buff[0] != 'B' || buff[1] != 'M')
    return 1;

  hdr->filesize = bmp_dword_value(&buff[2]);
  hdr->bitmap_offset = bmp_dword_value(&buff[10]);
  hdr->header_size = bmp_dword_value(&buff[14]);
  hdr->width = bmp_dword_value(&buff[18]);
  hdr->height = bmp_dword_value(&buff[22]);
  hdr->planes = bmp_word_value(&buff[26]);
  hdr->bpp = bmp_word_value(&buff[28]);
  hdr->compress = bmp_dword_value(&buff[30]);
  hdr->bitmap_size = bmp_dword_value(&buff[34]);
  hdr->colors = bmp_dword_value(&buff[46]);

  if (hdr->bpp == 8 && hdr->colors == 0)
    hdr->colors = 256;

  if (hdr->bpp == 4 && hdr->colors == 0)
    hdr->colors = 16;

  if (hdr->bpp == 1 && hdr->colors == 0)
    hdr->colors = 2;

  DEBUGF(("-- bitmap header --\n"));
  DEBUGF(("filesize = %u\n", (pu_long)hdr->filesize));
  DEBUGF(("bitmap_offset = %u\n", (pu_long)hdr->bitmap_offset));
  DEBUGF(("header_size = %u\n", (pu_long)hdr->header_size));
  DEBUGF(("width = %u\n", (pu_long)hdr->width));
  DEBUGF(("height = %u\n", (pu_long)hdr->height));
  DEBUGF(("planes = %u\n", (pu_long)hdr->planes));
  DEBUGF(("bpp = %u\n", (pu_long)hdr->bpp));
  DEBUGF(("compress = %u\n", (pu_long)hdr->compress));
  DEBUGF(("bitmap_size = %u\n", (pu_long)hdr->bitmap_size));
  DEBUGF(("colors = %u\n", (pu_long)hdr->colors));
  DEBUGF(("-------------------\n"));

  return 0;
}


static
int
bmp_get_palette(FILE *f, int colors, BMPPALETTE *pal)
{
  int i;
  pu_char *buff = NULL, *buff_p = NULL;
  size_t buffsize;

  if (colors == 0)
    return 1;

  pal->color_n = colors;
  pal->color = malloc(sizeof(PALETTE_COLOR) * colors);
  if (pal->color == NULL)
    return 1;
  buffsize = 4 * colors;
  buff = malloc(buffsize);
  if (buff == NULL)
    return 1;

  if (fseek(f, 54, SEEK_SET))
     return 1;

  if(fread(buff, 1, buffsize, f) != buffsize)
    return 1;
  read_pos += buffsize;

  DEBUGF(("-- palette --\n"));
  for(i = 0, buff_p = buff; i < colors; i++)
    {
      pal->color[i].blue = *buff_p++;
      pal->color[i].green = *buff_p++;
      pal->color[i].red = *buff_p++;
      buff_p++;
      DEBUGF(("color %2d : R(%02x) G(%02x) B(%02x)\n",
        i, pal->color[i].red, pal->color[i].green, pal->color[i].blue));
    }
  DEBUGF(("-------------\n"));

  free(buff);

  return 0;
}



/*
 * make frame
 */

static
void
set_frame_pixel(FRAMEINFO *frm, int x, int y, int black)
{
  int fi;
  int dx, dy, bx, mask;
  int p;

  if (!black)
    return;   /* already cleared */

  fi = (x / FRAME_WIDTH) + (y / FRAME_HEIGHT) * frm->framenum_x;

  /*DEBUGF(("put pixel (%3d, %3d) : frame = %d\n", x, y, fi));*/
  if (fi >= frm->framenum)
    return;

  dx = x % FRAME_WIDTH;
  dy = y % FRAME_HEIGHT;
  
  bx = dx / 8;
  mask = 0x80 >> (dx % 8);
  p = ((dy / 16) *  256) + bx * 16 + dy % 16;

  /*DEBUGF(("put pixel (%3d, %3d) : offset = %3d  mask = 0x%02x\n", x, y, p, mask));*/

  frm->frame[fi][p] |= (pu_char)mask;
}

int
bmp2frame(FILE *f, BMPHDR *hdr, BMPPALETTE *pal, FRAMEINFO *frm)
{
  pu_long bmp_size, line_size;
  pu_long read_len, line_len;
  int x, y;
  int i;

  if (hdr->bpp == 1)
    {
      line_size = (hdr->width + 7) / 8;
    }
  else if (hdr->bpp == 4)
    {
      line_size = (hdr->width + 1) / 2;
    }
  else if (hdr->bpp == 8)
    {
      line_size = hdr->width;
    }
  else if (hdr->bpp == 24)
    {
      line_size = hdr->width * 3;
    }
  else
    {
      return 1;
    }

  if (line_size % 4)
    line_size = line_size + 4 - line_size % 4;

  bmp_size = line_size * hdr-> height;

  DEBUGF(("bmp_size = %u  line_size = %u\n", bmp_size, line_size));

  if (fseek(f, hdr->bitmap_offset, SEEK_SET))
    return 1;

  read_len = 0;
  y = hdr->height;
  while(read_len < bmp_size)
    {
      line_len = 0;
      y--;
      x = 0;
      while(line_len < line_size)
        {
          if (hdr->bpp == 1)
            {
              PALETTE_COLOR *col;
              int byte, mask;
              if ((byte = fgetc(f)) == EOF)
                return 1;
              line_len++;
              read_len++;

              for (i = 0, mask = 0x80; i < 8; i++, mask >>= 1)
                {
                  col = &pal->color[(byte & mask) ? 1 : 0];
                  set_frame_pixel(frm, x, y, (col->red + col->green + col->blue != 255 * 3));
                  x++;
                }
            }
          else if (hdr->bpp == 4)
            {
              PALETTE_COLOR *col;
              int byte;
              if ((byte = fgetc(f)) == EOF)
                return 1;
              line_len++;
              read_len++;

              col = &pal->color[(byte & 0xf0) >> 4];
              set_frame_pixel(frm, x, y, (col->red + col->green + col->blue != 255 * 3));
              x++;
              col = &pal->color[(byte & 0x0f)];
              set_frame_pixel(frm, x, y, (col->red + col->green + col->blue != 255 * 3));
              x++;
            }
          else if (hdr->bpp == 8)
            {
              PALETTE_COLOR *col;
              int byte;
              if ((byte = fgetc(f)) == EOF)
                return 1;
              line_len++;
              read_len++;

              col = &pal->color[byte];
              set_frame_pixel(frm, x, y, (col->red + col->green + col->blue != 255 * 3));
              x++;
            }
          else if (hdr->bpp == 24)
            {
              int b1, b2, b3;
              if ((b1 = fgetc(f)) == EOF)
                return 1;
              if ((b2 = fgetc(f)) == EOF)
                return 1;
              if ((b3 = fgetc(f)) == EOF)
                return 1;
              line_len += 3;
              read_len += 3;

              set_frame_pixel(frm, x, y, (b1 + b2 + b3 != 255 * 3));
              x++;
            }
        }
    }

  return 0;
}


int
dump_frames(FILE *outf, FRAMEINFO *frm)
{
  int i;
  for(i = 0; i < frm->framenum; i++)
    {
      if (fwrite(frm->frame[i], 1, sizeof(FRAMEIMG), outf) != sizeof(FRAMEIMG))
        return 1;
    }

  return 0;
}


void
fput_word(FILE *f, int v)
{
  fputc(v & 0xff, f);
  fputc((v & 0xff00) >> 8, f);
}

int
dump_header(FILE *outf, ANIMINFO *anim, FRAMEINFO *frm)
{
  int i = 498;
  
  fputc('D', outf);
  fputc('S', outf);
  fput_word(outf, frm->framenum);
  fput_word(outf, anim->delay);
  fput_word(outf, (anim->loop_count == 0) ? anim->final_delay : anim->loop_count);
  fput_word(outf, 0x0104);
  fput_word(outf, anim->clear_lcd ? 1 : 0);
  fput_word(outf, (anim->loop_count != 0) ? 1 : 0);
  if (anim->comment != NULL)
  {
    fwrite(anim->comment, 1, strlen(anim->comment), outf);
    i = i - strlen(anim->comment);
  }
  for (i = i; i <= 498 ; i++)
  {
    fprintf(outf, "0");
  }

  return ferror(outf);
}



int
conv(FILE *f, FILE *outf, int op_framenum, ANIMINFO *anim)
{
  BMPHDR hdr;
  BMPPALETTE pal;
  FRAMEINFO frm;

  if (bmp_get_header(f, &hdr))
    {
      /* couldn't read bitmap header */
      return RIO_BMP2ANI;
    }

  if ( hdr.compress ||
       (hdr.bpp != 1 && hdr.bpp != 4 && hdr.bpp != 8 && hdr.bpp != 24) )
    {
      /* a type of this bitmap is not supported */
      return RIO_BMP2ANI;
    }

  if (hdr.colors != 0)
    if (bmp_get_palette(f, hdr.colors, &pal))
      {
        /* couldn't read palette */
        return RIO_BMP2ANI;
      }

  frm.framenum_x = hdr.width / FRAME_WIDTH;
  frm.framenum_y = hdr.height / FRAME_HEIGHT;
  frm.framenum = frm.framenum_x * frm.framenum_y;

  DEBUGF(("frame x = %d  y = %d\n", frm.framenum_x, frm.framenum_y));

  if (frm.framenum == 0)
    {
      /* no frame */
      return RIO_BMP2ANI;
    }
  if (frm.framenum >= 128)
    {
      /* too many frames */
      return RIO_BMP2ANI;
    }

  if (op_framenum > 0 && frm.framenum > op_framenum)
    frm.framenum = op_framenum;
  DEBUGF(("frames = %d\n", frm.framenum));

  frm.frame = malloc(sizeof(FRAMEIMG) * frm.framenum);
  if (frm.frame == NULL)
    return 1;
  memset(frm.frame, 0, sizeof(FRAMEIMG) * frm.framenum);

  if (bmp2frame(f, &hdr, &pal, &frm))
    {
      /* convert error */
      return RIO_BMP2ANI;
    }

  if (dump_header(outf, anim, &frm))
    {
      /* output error */
      return RIO_BMP2ANI;
    }

  if (dump_frames(outf, &frm))
    {
      /* output error */
      return RIO_BMP2ANI;
    }

  return RIO_SUCCESS;
}

int rio_bmp2rioani(int delay, int final_delay, int loop_count,
		gboolean clear_lcd, char *comment, char *bmpfile, char *outfile)
{
  FILE *f, *outf;
  ANIMINFO anim;
  int op_framenum;
  int r;

  /* parameters checking */
  if (delay > 20 || delay < 0
		  || final_delay < 1 || final_delay > 100
		  || loop_count > 99 || loop_count < 0
		  || bmpfile == NULL || outfile == NULL
		  || (comment != NULL && strlen(comment) > 498))
  {
	  return RIO_BMP2ANI;
  }

  /* Setting up the animation parameters */
  op_framenum = 0;
  anim.delay = delay;
  anim.final_delay = final_delay;
  anim.loop_count = loop_count;
  anim.clear_lcd = clear_lcd;
  anim.comment = g_strdup(comment);

#ifdef DEBUG
  g_print("delay: %d\n", anim.delay);
  g_print("final_delay: %d\n", anim.final_delay);
  g_print("loop_count: %d\n", anim.loop_count);
  g_print("clear_lcd: %d\n", anim.clear_lcd);
  g_print("comment: %s\n", anim.comment);
#endif

  f = fopen(bmpfile, "r");
  if (f == NULL)
    {
      return RIO_NOTANIM;
    }

  outf = fopen(outfile, "w");
  if (outf == NULL)
    {
      fclose(f);
      return RIO_NOTANIM;
    }

  r = conv(f, outf, op_framenum, &anim);

  fclose(f);
  fclose(outf);

  return r;
}

