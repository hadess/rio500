/*
 *  Copyright (C) 2001  Bastien Nocera
 *  Copyright (C) 2000  Iwasa Kazmi
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "config.h"

#include <glib/gi18n-lib.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

#include "librio500_api.h"

/* Private functions prototypes */
static int rio_set_anim_helper (Rio500 *rio, FILE *animf);
static int rio_remove_anim_helper (Rio500 *rio);

static int
rio_set_anim_helper(Rio500 *rio, FILE *animf)
{
  unsigned long rev;
  long fsize;
  char *buff = NULL;
  int r, len;

  fseek(animf, 0, SEEK_END);
  fsize = ftell(animf);
  fseek(animf, 0, SEEK_SET);

  rev = query_firmware_rev(rio->rio_dev);
  if ((rev & 0xff00) == 0x0100)
    {
      if (fsize > (48*1024))
      {
          finish_communication(rio->rio_dev);
          return RIO_FIRM_1X;
      }
    }
  else
   {
      /* firmware rev 2.xx */
      if (fsize > (64*1024))
        {
          finish_communication(rio->rio_dev);
          return RIO_FIRM_1X;
        }
    }


  buff = malloc(0x1000);
  if (!buff)
    {
      return PC_MEMERR;
    }
  if (rio->stat_func)
	  (*rio->stat_func) (0, _("Setting animation."),0);

  /* start xfer */
  send_command (rio->rio_dev, 0x3f, 0x0, fsize);
  len = 0;
  while(len < fsize)
    {
      r = fread(buff, 1, 0x1000, animf);
      if (r == 0)
        break;
      bulk_write(rio->rio_dev, buff, r);
      len += r;
    }
   
  free(buff);

  return RIO_SUCCESS;
}

int
rio_remove_anim_helper(Rio500 *rio)
{
  char buff[4] = { 0x30, 0x30, 0x30, 0x00 };

  if (rio->stat_func)
	  (*rio->stat_func) (0, _("Removing Animation."),0);
  send_command (rio->rio_dev, 0x3f, 0x0, 4);
  bulk_write(rio->rio_dev, buff, 4);

  return RIO_SUCCESS;
}

/*
 * returns the comment of the animation
 * filename is the path to the animation
 */

char *
rio_anim_comment(char *filename)
{
	FILE *animf = NULL;
	int c1, c2;
	int ret;
	char *buff;
	
	animf = fopen(filename, "r");

	if (!animf)
	{
		return NULL;
	}

	c1 = fgetc(animf);
	c2 = fgetc(animf);

	if (c1 != 'D' || c2 != 'S')
	{
		fclose(animf);
		return NULL;
	}

	buff = malloc(498);
	memset(buff, 0, sizeof(buff));

	if (fseek(animf, 14, SEEK_SET))
	{
		g_free(buff);
		return NULL;
	}

	ret = fread(buff, 1, 498, animf);
	if (ret != 498)
	{
		g_free(buff);
		return NULL;
	}

	return buff;
}

static int rio_anim_helper(Rio500 *rio, char *filename, gboolean remove)
{
  FILE *animf = NULL;
  int c1, c2;
  int r;

  if (remove == FALSE)
  {
      animf = fopen(filename, "r");
      if (!animf)
      {
          return RIO_NOTANIM;
      }
      c1 = fgetc(animf);
      c2 = fgetc(animf);
      if (c1 != 'D' || c2 != 'S')
      {
          fclose(animf);
          return RIO_NOTANIM;
      }
  }

  /* Start the transfer */
  if (rio->stat_func)
	  (*rio->stat_func) (0, _("Opening rio device..."),0);
  init_communication (rio->rio_dev);

  send_command (rio->rio_dev, 0x42, 0, 0);
  send_command (rio->rio_dev, 0x42, 0, 0);

  /* Updating the animation */

  if (remove)
    {
      r = rio_remove_anim_helper(rio);
    }
  else
    {
      r = rio_set_anim_helper(rio, animf);
    }

  send_command (rio->rio_dev, 0x42, 0, 0);
  send_command (rio->rio_dev, 0x42, 0, 0);

  /* Finishing the transfer */
  if (rio->stat_func)
	  (*rio->stat_func) (0, _("Communication finished."),0);
  finish_communication (rio->rio_dev);


  if (animf)
    fclose(animf);

  return r;
}

int
rio_set_anim (Rio500 *rio, gchar *filename)
{
	return rio_anim_helper(rio, filename, FALSE);
}

int rio_remove_anim (Rio500 *rio)
{
	return rio_anim_helper(rio, NULL, TRUE);
}
