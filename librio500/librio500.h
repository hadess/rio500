/*  ----------------------------------------------------------------------

    Copyright (C) 2000  Cesar Miquel  (miquel@df.uba.ar)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    ---------------------------------------------------------------------- */


/* 
    -------------------
    Function prototypes 
    -------------------
*/

#include <glib.h>

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/ioctl.h>
#include <usb.h>


#ifndef LIBRIO500_H
#define LIBRIO500_H

typedef guint16 WORD;
typedef guint8	BYTE;
typedef guint32 DWORD;
typedef char string[256];

/* RIO COMMANDS */

#define READ_FROM_USB               0x45
#define WRITE_TO_USB                0x46
#define START_USB_COMM              0x47
#define END_USB_COMM                0x48
#define RIO_FORMAT_DEVICE           0x4d
#define QUERY_FREE_MEM              0x50
#define QUERY_OFFSET_LAST_WRITE     0x43
#define SEND_FOLDER_LOCATION        0x56
#define END_FOLDER_TRANSFERS        0x58

#define FOLDER_BLOCK_SIZE           0x4000

#define BUFFER_SIZE                 0x4000

#define BULK_WRITE_TIMEOUT          5000
#define BULK_READ_TIMEOUT           5000
#define CONTROL_TIMEOUT             5000

#define BULK_READ_ENDPOINT          0x81
#define BULK_WRITE_ENDPOINT         0x02

#define STR_RIFF (((((('R' << 8) | 'I') << 8) | 'F') << 8) | 'F')
#define STR_WAVE (((((('W' << 8) | 'A') << 8) | 'V') << 8) | 'E')
#define STR_MPEG (((((('M' << 8) | 'P') << 8) | 'E') << 8) | 'G')
#define STR_fmt  (((((('f' << 8) | 'm') << 8) | 't') << 8) | ' ')
#define STR_fact (((((('f' << 8) | 'a') << 8) | 'c') << 8) | 't')
#define STR_data (((((('d' << 8) | 'a') << 8) | 't') << 8) | 'a')

typedef struct
{
  WORD    num_blocks;
  BYTE    bitmap[1536];
} rio_bitmap_data;

typedef struct
{
  WORD             offset;
  WORD             dunno1;
  DWORD            length;
  WORD             dunno2;
  WORD             dunno3;
  DWORD            mp3sig;
  DWORD            time;
  rio_bitmap_data bitmap; 
  BYTE             name1[362];
  BYTE             name2[128];
} song_entry;


typedef struct
{
  WORD            offset;
  WORD            dunno1;
  WORD            fst_free_entry_off;
  WORD            dunno2;
  DWORD           dunno3;
  DWORD           dunno4;
  DWORD           time;
  rio_bitmap_data bitmap; 
  BYTE            name1[362];
  BYTE            name2[128];
} folder_entry;

typedef struct
{
  WORD            dunno1;
  WORD            block_size;
  WORD            num_blocks;
  WORD            first_free_block;
  WORD            num_unused_blocks;
  DWORD           dunno2;
  DWORD           dunno3;
} mem_status;

typedef struct
{
  WORD            offset;
  WORD            bytes;
  WORD            folder_num; 
} folder_location;


/* functions order from high-level to low-level */
mem_status    *get_mem_status (usb_dev_handle *rio_dev, int card);
unsigned long  query_mem_left (usb_dev_handle *rio_dev, int card);
unsigned long  query_card_count (usb_dev_handle *rio_dev);
unsigned long  query_firmware_rev (usb_dev_handle *rio_dev);
void           send_folder_location (usb_dev_handle *rio_dev, int offset, int folder_num, int card);
void           format_flash (usb_dev_handle *rio_dev, int card);
usb_dev_handle *init_communication ();
void           finish_communication (usb_dev_handle *rio_dev);

void  bswap_folder_entry(folder_entry *);
void  bswap_song_entry(song_entry *);
GList *read_folder_entries (usb_dev_handle *rio_dev, int card);
GList *read_song_entries (usb_dev_handle *rio_dev, GList *folder_entries, int folder_num, int card);
void   write_folder_entries (usb_dev_handle *rio_dev, GList *entries, int card);
void   write_song_entries (usb_dev_handle *rio_dev, int folder_num, GList *entries, int card);

unsigned long  send_command (usb_dev_handle *rio_dev, int req, int value, int index);
unsigned long  send_read_command (usb_dev_handle *rio_dev, int address, int num_blocks, int card);
unsigned long  send_write_command (usb_dev_handle *rio_dev, int address, int num_blocks, int card);

int rio_ctl_msg (usb_dev_handle *rio_dev, int dir, int req, int val, int idx, int len, void *d);
int bulk_read (usb_dev_handle *rio_dev, BYTE *block, int num_bytes);
int bulk_write (usb_dev_handle *rio_dev, BYTE *block, int num_bytes);
int rio_usb_bulk (usb_dev_handle *rio_dev, int ep, void *block, int len, int direction);
void dump_block (FILE *fp, BYTE *block, int num_bytes);
int swap_songs (usb_dev_handle *rio_dev, int folder_num, int song_1, int song_2, int rio_card);

song_entry    * song_entry_new (char *name, char *font_name, int font_number);
folder_entry  * folder_entry_new (char *name, char *font_name, int font_number);
rio_bitmap_data  * bitmap_data_new (char *name, char *font_name, int font_number);
BYTE             * new_empty_block ();
void               clear_block (BYTE *block);
unsigned long get_frame_header(FILE *fp);
int is_frame_header(unsigned long fh);
int mp3_read_long(unsigned long *fhp, FILE *fp);
int mp3_read_byte(unsigned long *fhp, FILE *fp);


/* safe_strcpy and safe_strcat from samba */
char *safe_strcpy(char *dest,const char *src, size_t maxlength);
char *safe_strcat(char *dest, const char *src, size_t maxlength);



#endif /* LIBRIO500_H */
