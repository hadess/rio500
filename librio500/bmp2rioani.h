/*
 *  Rio500 Startup Animation Data Converter
 *
 *  Copyright (C) 2001  Bastien Nocera
 *  Copyright (C) 2000  Iwasa Kazmi
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * 2000/08/05  v0.3
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <librio500_api.h>

typedef unsigned long pu_long;
typedef unsigned char pu_char;

typedef struct tANIMINFO
{
  int delay;
  int final_delay;
  int loop_count;
  int clear_lcd;

  char *comment;

} ANIMINFO;



#define FRAME_WIDTH 128
#define FRAME_HEIGHT 32

typedef pu_char FRAMEIMG[512];

typedef struct tFRAMEINFO
{
  int framenum_x;
  int framenum_y;
  int framenum;

  FRAMEIMG *frame;

} FRAMEINFO;


/* This is the main function of bmp2rioani
 * delay:
 * 	Delay between frames in 1/20 of ms (0 to 20)
 * 	default 1
 * final_delay:
 * 	Delay of final frame in 1/20 of ms (1 to 100)
 * 	default 4
 * loop_count:
 * 	Number of times the animation should loop (0 or 1 to 99)
 * 	Setting to >0 disables final frame delay
 * 	default 0
 * clear_lcd:
 * 	Clear the screen after the animation ? (0 or 1)
 * 	default 0
 * comment:
 * 	string of up to 499 characters
 * bmpfile:
 * 	path to the input file
 * outfile:
 * 	path to the output file
 */
int rio_bmp2rioani(int delay, int final_delay, int loop_count,
		gboolean clear_lcd, char *comment,
		char *bmpfile, char *outfile);

